use std::rc::Rc;
use std::cell::RefCell;

fn precedes(c1: char, c2: char) -> bool {
	let priority = |c: char| {
		match c {
			'|' => 4,
			'.' => 3,
			'(' | ')' => 1,
			'*' | '+' | '?' => 0,
			_ => 0,
		}
	};

	let p1 = priority(c1);
	let p2 = priority(c2);

	p1 - p2 >= 0
}

fn re_to_postfix(re: &str) -> String {
	let mut chars = re.chars();
	let mut postfix = String::new();
	let mut t = Vec::new();
	t.push(String::new());

	let mut shunt = |c: char, t: &mut Vec<String>| {
		if c == '(' {
			t.push(String::new());
		} else if c == ')' {
			while let Some(c) = t.last_mut().unwrap().pop() {
				postfix.push(c);
			}
			t.pop();
			return;
		}

		while let Some(top) = t.last().unwrap().chars().rev().next() {
			if precedes(c, top) {
				postfix.push(t.last_mut().unwrap().pop().unwrap());
			} else {
				break;
			}
		}
		
		if c != '(' {
			t.last_mut().unwrap().push(c);
		}
	};

	let mut last = None;

	while let Some(c) = chars.next() {
		if let Some(l) = last {
			if l != '|' && l != '(' && c != '*' && c != '+' && c != '?' &&  c != '|' && c != ')' {
				shunt('.', &mut t);
			}
		}

		shunt(c, &mut t);
		last = Some(c);
	}	

	assert!(t.len() == 1, "Brackets not balanced");

	while let Some(c) = t.last_mut().unwrap().pop() {
		postfix.push(c);
	}

	postfix
}


struct State {
	symbol: Option<char>,
	edge1: Option<Rc<RefCell<State>>>,
	edge2: Option<Rc<RefCell<State>>>,
	last: bool,
}

impl State {
	fn new(s: Option<char>) -> Self {
		State {
			symbol: s,
			edge1: None,
			edge2: None,
			last: false,
		}
	}

	fn last_state(&mut self) {
		self.last = true;
	}
}

struct Nfa {
	initial: Option<Rc<RefCell<State>>>,
	accept:	Option<Rc<RefCell<State>>>,
	current: Option<Vec<Rc<RefCell<State>>>>,
}

impl Nfa {
	fn new(re: &str) -> Self {
		let postfix = re_to_postfix(re);
		let mut chars = postfix.chars();
		let mut nfas: Vec<Self> = Vec::new();

		while let Some(c) = chars.next() {
			match c {
				'.' => {
					let mut frag2 = nfas.pop().unwrap();
					let mut frag1 = nfas.pop().unwrap();

					frag1.accept.unwrap().borrow_mut().edge1 = Some(Rc::clone(&frag2.initial.unwrap()));

					nfas.push(Nfa {
						initial: frag1.initial,
						accept: frag2.accept,
						current: None,
					});
				},
				'|' => {
					let mut frag2 = nfas.pop().unwrap();
					let mut frag1 = nfas.pop().unwrap();

					let mut initial = State::new(None);
					initial.edge1 = Some(Rc::clone(&frag1.initial.unwrap()));
					initial.edge2 = Some(Rc::clone(&frag2.initial.unwrap()));

					let accept = Rc::new(RefCell::new(State::new(None)));
					frag1.accept.unwrap().borrow_mut().edge1 = Some(Rc::clone(&accept));
					frag2.accept.unwrap().borrow_mut().edge1 = Some(Rc::clone(&accept));

					nfas.push(Nfa {
						initial: Some(Rc::new(RefCell::new(initial))),
						accept: Some(accept),
						current: None,
					});
				},
				'*' => {
					let mut frag = nfas.pop().unwrap();
					
					let accept = Rc::new(RefCell::new(State::new(None)));

					let mut initial = State::new(None);
					initial.edge1 = Some(Rc::clone(frag.initial.as_ref().unwrap()));
					initial.edge2 = Some(Rc::clone(&accept));

					let initial_ref = Rc::new(RefCell::new(initial));

					frag.accept.as_ref().unwrap().borrow_mut().edge1 = Some(Rc::clone(&initial_ref));
					
					nfas.push(Nfa {
						initial: Some(initial_ref),
						accept: Some(accept),
						current: None,
					});
				},
				'+' => {
					let mut frag = nfas.pop().unwrap();

					let accept = Rc::new(RefCell::new(State::new(None)));
					
					let mut initial = State::new(None);
					initial.edge1 = Some(Rc::clone(frag.initial.as_ref().unwrap()));
					initial.edge2 = Some(Rc::clone(&accept));

					frag.accept.as_ref().unwrap().borrow_mut().edge1 = Some(Rc::new(RefCell::new(initial)));
					
					nfas.push(Nfa {
						initial: frag.initial,
						accept: Some(accept),
						current: None,
					});
				}
				'?' => {
					let mut frag = nfas.pop().unwrap();

					let accept = Rc::clone(frag.accept.as_ref().unwrap());

					let mut initial = State::new(None);
					initial.edge1 = Some(Rc::clone(frag.initial.as_ref().unwrap()));
					initial.edge2 = Some(accept);
					
					nfas.push(Nfa {
						initial: Some(Rc::new(RefCell::new(initial))),
						accept: frag.accept,
						current: None,
					});
				},
				_ => {
					let accept = Rc::new(RefCell::new(State::new(None)));

					let mut initial = State::new(Some(c));
					initial.edge1 = Some(Rc::clone(&accept));

					nfas.push(Nfa {
						initial: Some(Rc::new(RefCell::new(initial))),
						accept: Some(accept), 
						current: None,
					})
				}
			}
		}

		assert!(nfas.len() == 1);
		
		let mut nfa = nfas.pop().unwrap();
		nfa.accept.as_ref().unwrap().borrow_mut().last_state();
		nfa
	}

	fn next_states(s: &Rc<RefCell<State>>) -> Vec<Rc<RefCell<State>>> {
		let mut states: Vec<Rc<RefCell<State>>> = Vec::new();
		states.push(Rc::clone(s));

		if !s.borrow().last && !s.borrow().symbol.is_some() {
			let t = s.borrow();
			let e1 = t.edge1.as_ref().unwrap();
			states.extend(Nfa::next_states(e1).into_iter());

			if s.borrow().edge2.is_some() {
				let t = s.borrow();
				let e2 = t.edge2.as_ref().unwrap();
				states.extend(Nfa::next_states(e2).into_iter());
			}
		}

		states
	}


	fn is_match(&self, s: &str) -> bool {
		let mut chars = s.chars();
		let initial = Rc::clone(self.initial.as_ref().unwrap());
		let mut current = Nfa::next_states(&initial);

		while let Some(c) = chars.next() {
			let mut next = Vec::new();

			for state in current.iter() { 
				if let Some(s) = state.borrow().symbol {
					if c == s {
						let t = state.borrow();
						let e1 = t.edge1.as_ref().unwrap();
						next.extend(Nfa::next_states(e1).into_iter());
					}
				}
			}

			current = next;
		}

		for c in current.iter() {
			if c.borrow().last == true {
				return true;
			}
		}

		false
	}

	fn next_match(&mut self, c: char) -> bool {
		if !self.current.is_some() {
			let initial = Rc::clone(self.initial.as_ref().unwrap());
			self.current = Some(Nfa::next_states(&initial));
		}

		let mut next = Vec::new();

		for state in self.current.as_ref().unwrap().iter() { 
			if let Some(s) = state.borrow().symbol {
				if c == s {
					let t = state.borrow();
					let e1 = t.edge1.as_ref().unwrap();
					next.extend(Nfa::next_states(e1).into_iter());
				}
			}
		}
		
		if next.len() > 0 {
			self.current = Some(next);
			true
		} else {
			false
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn to_postfix() {
		assert_eq!(String::from("ab|"), re_to_postfix("a|b"));
		assert_eq!(String::from("ab.c|"), re_to_postfix("ab|c"));
		assert_eq!(String::from("abc|."), re_to_postfix("a(b|c)"));
		assert_eq!(String::from("abcd||."), re_to_postfix("a(b|(c|d))"));
		assert_eq!(String::from("abc|*."), re_to_postfix("a(b|c)*"));
		assert_eq!(String::from("abc*|."), re_to_postfix("a(b|c*)"));
		assert_eq!(String::from("a*b."), re_to_postfix("a*b"));
		assert_eq!(String::from("ab*|"), re_to_postfix("a|b*"));
		assert_eq!(String::from("a*b*|"), re_to_postfix("a*|b*"));
		assert_eq!(String::from("ab|*"), re_to_postfix("(a|b)*"));
		assert_eq!(String::from("ab*|"), re_to_postfix("a|(b*)"));
		assert_eq!(String::from("a+b?.c.d|"), re_to_postfix("(a+b?c)|d"));
	}

	#[test]
	fn nfa_match() {
		let nfa = Nfa::new("a|b");
		assert_eq!(true, nfa.is_match("a"));
		assert_eq!(true, nfa.is_match("b"));

		let nfa = Nfa::new("a*|b");
		assert_eq!(false, nfa.is_match("aaaaaab"));
		assert_eq!(true, nfa.is_match("aaaa"));
		assert_eq!(true, nfa.is_match("b"));
		
		let nfa = Nfa::new("a*b");
		assert_eq!(true, nfa.is_match("ab"));
		assert_eq!(false, nfa.is_match("a"));
		assert_eq!(false, nfa.is_match("aaaa"));

		let nfa = Nfa::new("ab");
		assert_eq!(false, nfa.is_match("a"));

		let nfa = Nfa::new("a(b|c)");
		assert_eq!(true, nfa.is_match("ab"));
		assert_eq!(true, nfa.is_match("ac"));
		assert_eq!(false, nfa.is_match("abc"));

		let nfa = Nfa::new("a*b");
		assert_eq!(true, nfa.is_match("aaaaab"));

		let nfa = Nfa::new("a+b");
		assert_eq!(true, nfa.is_match("aaaaab"));

		let nfa = Nfa::new("a?b");
		assert_eq!(true, nfa.is_match("b"));
	}

	#[test]
	fn nfa_match_next() {
		let mut nfa = Nfa::new("a*|b*");
		assert_eq!(true, nfa.next_match('a'));
		assert_eq!(true, nfa.next_match('a'));
		assert_eq!(false, nfa.next_match('b'));
		assert_eq!(false, nfa.next_match('b'));
	}
}
